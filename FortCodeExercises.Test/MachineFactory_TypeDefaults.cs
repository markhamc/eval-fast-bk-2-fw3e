using System;
using Xunit;
using FortCodeExercises.Exercise1;

namespace FortCodeExercises.Test.Exercise1
{
    public class MachineFactory_TypeDefaults
    {
        [Theory]
        [InlineData(0, " red bulldozer [80].")]
        [InlineData(1, " blue crane [75].")]
        [InlineData(2, " green tractor [90].")]
        [InlineData(3, " yellow truck [70].")]
        [InlineData(4, " brown car [70].")]
        public void MachineFactory_ForTypes_ProducesCorrectDescription(int type, string expected)
        {
            var machine = new MachineFactory().CreateDefaultMachine(type);
            Assert.Equal(expected, machine.description);
        }

        [Theory]
        [InlineData(0, "")]
        [InlineData(1, "white")]
        [InlineData(2, "gold")]
        [InlineData(3, "")]
        [InlineData(4, "")]
        public void MachineFactory_ForTypes_ProducesCorrectTrimColor(int type, string expected)
        {
            var machine = new MachineFactory().CreateDefaultMachine(type);
            Assert.Equal(expected, machine.trimColor);
        }
    }
}
