using System;
using Xunit;
using FortCodeExercises.Exercise1;

namespace FortCodeExercises.Test.Exercise1
{
    public class Machine_MutableProperties
    {
        [Theory]
        [InlineData(0, "bulldozer", 80, "red", "", "bulldozer")]
        [InlineData(1, "crane", 75, "blue", "white", "crane")]
        [InlineData(2, "tractor", 90, " green", "gold", "tractor")]
        [InlineData(3, "truck", 70, " yellow", "", "truck")]
        [InlineData(4, "car", 70, "brown", "", "car")]
        public void Machine_ConstructWithName_ReturnsName(int type, string machineName, int maxSpeed, string color, string trimColor, string expected)
        {
            var machine = new MachineFactory().CreateDefaultMachine(type);
            Assert.Equal(expected, machine.name);
        }

        [Theory]
        [InlineData(0, "Lightning")]
        [InlineData(1, "Mator")]
        [InlineData(2, "Larry")]
        [InlineData(3, "Sally")]
        [InlineData(4, "Jessie")]
        public void Machine_UpdateName_ReturnsName(int type, string name)
        {
            var machine = new MachineFactory().CreateDefaultMachine(type);
            machine.machineName = name;
            Assert.Equal(name, machine.name);
        }
    }
}
