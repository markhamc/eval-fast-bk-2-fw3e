# Assumptions

Absent any documentation or calling code, several assumptions are made regarding the intent of the code.

- Machine consists of the following properties:
    - Name/MachineName
    - Type
    - Color
    - TrimColor
    - MaxSpeed
- Additionally, a getter for `description` which is computed based on the other properties
- Type is used to set the default values
    - This keeps the current behavior intact while allowing the most flexibility
    - In the original implementation, setting a non-empty `machineName` would cause the `name` property to return an empty string. This apparent bug is fixed to allow overriding the default name.
- All properties aside from name should be immutable
- `isDark` should not be part of the Machine class
    - It is used to set the trim color based on values of type and color
    - 'Dark' should be a property of the color or perhaps a helper method in a trim color lookup
    - Removed entirely since it does not affect the computed trim values
- Removed unused `absoluteMax` variable in `getMaxSpeed`.
- Removed `hasMaxSpeed` as it serves no purpose because it is tied directly to `type`.
- There are numerous unreachable branches of the code, these are removed.

# Approach

One initial thought was to create classes for each machine type, much like a textbook object-oriented example.
I chose not to do this, because it is the most rigid approach that would be impossible to extend without modifying the code.

Instead, I chose to use the values from the code as defaults for a factory that creates fully-specified machines.
This allows the most flexibility. For this implementation, default values will be stored as code within the factory. As needed, the factory could be replaced with some other mechanism (templates stored in a database, for example).

Added unit tests to ensure the original behavior was preserved. These can be run by calling `dotnet test` in the solution folder.

# Future work

- The naming style should be modified to conform to common C#/.NET practices (e.g., capitals for method names)
    - This may break existing users of this class, so I am hesitant to implement this without that additional information.
- Add additional unit tests for the behavior of the Machine class
    - As is stands, there is not really any behavior in the class so it's not worth testing beyond the MachineFactory tests
- Create a class for `Color`. This might include the `isDark` behavior as discussed above. It could include the `baseColor` and `trimColor` or an instance of `Color` might be used for each separately. There is not current enough information about how colors will be used to justify the extra complexity.
- Create a class or enum for `type`. Using an enum would limit the types to what is defined in code. Instead, there would most likely be an entity for `MachineType` that stored the id and name in a database table.