namespace FortCodeExercises.Exercise1
{
    public class Machine
    {
        public string machineName { get; set; }
        public int type { get; }
        public int maxSpeed { get; }
        public string color { get; }
        public string trimColor { get; }
        public string name => machineName;
        public string description => $" {this.color} {this.name} [{this.maxSpeed}].";

        public Machine(int type, string machineName, int maxSpeed, string color, string trimColor)
        {
            this.type = type;
            this.machineName = machineName;
            this.maxSpeed = maxSpeed;
            this.color = color;
            this.trimColor = trimColor;
        }

    }
}