namespace FortCodeExercises.Exercise1
{
    public class MachineFactory
    {
        public Machine CreateDefaultMachine(int type)
        {
            return DefaultValues(type);
        }

        private Machine DefaultValues(int type) => type switch
        {
            0 => new Machine(type, "bulldozer", 80, "red", ""),
            1 => new Machine(type, "crane", 75, "blue", "white"),
            2 => new Machine(type, "tractor", 90, "green", "gold"),
            3 => new Machine(type, "truck", 70, "yellow", ""),
            4 => new Machine(type, "car", 70, "brown", ""),
            _ => throw new System.NotImplementedException()
        };
    }
}